import Vue from 'vue';
import Question from '@/components/Question.vue';
import QuestionTitle from '@/components/QuestionTitle.vue';
import QuestionBody from '@/components/QuestionBody.vue';
import QuestionActual from '@/components/QuestionActual.vue';
import QuestionDescription from '@/components/QuestionDescription.vue';
import RightAnswer from '@/components/RightAnswer.vue';
import AnswerList from '@/components/AnswerList.vue';
import AnswerItem from '@/components/AnswerItem.vue';

[
  Question,
  QuestionTitle,
  QuestionBody,
  QuestionActual,
  QuestionDescription,
  RightAnswer,
  AnswerList,
  AnswerItem,
]
  .forEach(component => Vue.component(component.name, component));
