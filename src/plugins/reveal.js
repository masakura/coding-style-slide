import Reveal from 'reveal.js';
import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/black.css';

export default {
  initialize() {
    Reveal.initialize();
    Reveal.addEventListener('slidechanged', (event) => {
      event.currentSlide
        .querySelectorAll('.repl-it')
        .forEach(el => el.init());

      document.querySelectorAll('dialog')
        .forEach(dialog => dialog.close());
    });
  },
};
