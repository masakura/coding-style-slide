import Vue from 'vue';
import reveal from './plugins/reveal';
import './plugins/highlight';
import './plugins/repl-it';
import './plugins/common';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  render(h) {
    return h(App);
  },
  mounted() {
    reveal.initialize();
  },
}).$mount('#app');
